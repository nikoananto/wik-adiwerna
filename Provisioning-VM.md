#### Folder Tfgen
Folder ini berisi script-script yang digunakan untuk provisioning dengan terraform, setiap materi membutuhkan spesfikasi cloud init yang berbeda.
```
rh124 -> rha-tf
rh134 -> rha-tf
rh294 -> rh294-tf
```

#### Pre Provisioning
1\. Membuat kvm network <br>
Sebelum provisioning kita membutuhkan kvm network di setiap host, untuk mempermudah bisa menggunakan script seperti dibawah:
```
$ ./net-20.sh 1 38
```
ini akan menggenerate network untuk absen 1 hingga 38. Jalankan di setiap host.

#### Provisioning
1\. **prepare.sh** <br>
Script ini digunakan untuk menggenerate folder terraform yang berisi tf file. Script ini hanya menggenerate code dan tidak melakukan provisioning apapun. 
Cara mengunakan script ini:
```
$ ./prepare.sh
host: (tulis host dimana vm akan di provisioning, bisa hosts atau ip, contoh : adw2 atau 10.1.1.12
first pod : (absen pod pertama yang akan di generate terraform folder nya)
last pod : (absen pod terakhr yang akan di generate terraform folder nya)
```

2\. **prov.sh** <br>
Script ini akan meng inisiasi terraform dan mengaplikasikan tf file nya (provisioning). Pastikan pod yang akan diprovisioning sudah digenerate tf foldernya dengan `prepare.sh`
Cara menggunakan script ini:
```
$ ./prov.sh
first pod: 1
first pod: 36
```
Note: untuk provisioning usahakan jangan langsung banyak, bisa lima lima dulu. Misal 1 kelas 36 pod, nah bisa provisioning 1-5 vm dlu, jika cpu udah turun baru lanjut provisioning 6-10 dan seterusnya.

3\. **destroy.sh** <br>
Script ini digunakan untuk mendestroy vm/pod.
Cara menggunakan script ini:
```
$ ./destroy.sh
first pod: 1
first pod: 5
Semua resource pod akan dihapus dan tidak bisa direstore, ketik 'yes' jika yakin:" yes
```
Note: hati-hati jika menggunakan script ini, karena jika sudah di destory state sebelumnya tidak bisa direstore

4\. **rebuild.sh** <br>
Jika ada vm yang error saat proses provisioning atau yang lain, kita bisa merebuildnya
```
$ ./rebuild.sh
podnumber: 1
```
script di atas akan mendestroy dan mengapply ulang (rebuild)

#### Port Forwarding
Karena pod tersebar dibeberapa host, disini kita menggunakan 2 kali forwarding agar bisa di ssh dari 1 host yaitu adw1. Untuk mempermudah port forwarding bisa menggunakan script `iptables-ssh.sh`

Mapping pod yang tersebar di setiap host, edit iptables-ssh.sh
```
adw1=(1 8)
adw2=(9 18)
adw3=(19 23)
adw4=(24 28)
adw5=(29 30)
adw6=(31 32)
adw7=(33 36)
adw8=(37 60)
```
Contoh adw1 ada pod 1-8, di adw2 pod 9-18 dan seterusnya.

Simpan dan jalankan iptables-ssh.sh
```
./iptables-ssh.sh
```
