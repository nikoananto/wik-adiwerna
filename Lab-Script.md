#### Example script
```bash
#!/bin/bash

PATH=/usr/bin:/bin:/usr/sbin:/sbin

# Initialize and set some variables
run_as_root='true'
target='workstation'

declare -a valid_commands=(start grade finish)

function lab_start {

  print_header 'Starting lab.'

  print_line "Preparing ${target} for lab exercise work:"
  print_line

  pad ' · Ensuring a clean lab environment'
  cmd1="echo student | passwd --stdin student"
  if ${cmd1}; then
    print_SUCCESS
  else
    print_FAIL
  fi  
}

function lab_grade {

  print_header "Grading the student's work on ${target}:"
  #host_reachable ${target}

  somecheck=$(cat bla bla)

  pad ' · if '
  if [[ ${somecheck} = "some-answer" ]]
  then
    print_PASS
  else
    print_FAIL
  fi

  # Overall grade
  print_line
  pad 'Overall lab grade'
  if [[ ${fail_count} -eq 0 ]]
  then
    print_PASS
  else
    print_FAIL
  fi

  assessment

  print_line
}

function lab_finish {

  print_header "Cleaning up the lab on ${target}:"
  pad " · Removing the files created"
  rm -rf ${somefile}
  if ! -f ${somefile}; then
    print_SUCCESS
  else
    print_FAIL
  fi  

  print_line 'Lab finished.'
  print_line
}

############### Don't EVER change anything below this line ###############

# Source library of functions
source /usr/local/lib/${function_lib}
source /usr/local/lib/${platform_lib}

grading_main_program "$@"
```

#### Predefined Function
Berikut beberapa function yang bisa diganakan di lab script (contoh pengunaanya dbisa dilihat di example script di atas)

1\. **Output function**

Beberapa function ini akan membuat output (print) suatu text ke terminal user.
![print](./uploads/print_function.png)

2\. **Scoring Function**

function print_PASS dan print_FAIL digunakan untuk print output SUCCESS/FAIL dan juga mengincrease 1 point ke variable `pass_count` atau `fail_count` yang nantinya variable ini akan dijadikan parameter saat push ke scoring API. 2 Function ini digunakan di lab grade.

![grade](./uploads/grade.png)

3\. **Assessment Function**

function assessment ini dipanggil saat lab grade, function ini akan melakukan `http POST request` ke `scoring API` dengan parameter seperti kelas, absen, course, pass_count, fail_count dll.


#### Mengupdate Lab Script
Untuk mengupdate lab script bisa langsung mengubahnya di repository course (rh124, rh134, rh194 dst), ketika commit perubahan, akan otomatis update juga di server reponya karena sudah terintegrasi CI. Setelah mengupdate di repoistory ini, jangan lupa untuk update di sisi student workstationnya, dengan command
```
lab update
```

#### Membuat Custom Lab Script
Berikut cara untuk  membuat custom lab script:
1. Copy template contoh script diatas, lalu buat file di repository course (rh124, rh134, rh194 dst) dengan nama `lab-namalab`
2. Sesuaikan lab start, grade, dan finisihnya
3. Commit dan push
4. Buat `kolom` di sheet dimana custom lab tersebut berada, dengan nama `namalab`.
5. Update dari sisi siswa dengan perintah `lab update`
6. note: ubah `namalab` dengan nama lab yang diinginkan
