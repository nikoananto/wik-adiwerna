**SETUP KVM**
1. Install KVM and associate packages<br>
`yum -y group install 'Virtualization Host'`
2. Start and enable the libvirtd service<br>
`systemctl enable --now libvirtd`
3. Check KVM module is loaded or not<br>
`lsmod | grep kvm`
4. Create pool isos & pool vms <br>
`virsh pool-define-as --name isos --type dir --target /data/isos`<br>
`virsh pool-define-as --name vms --type dir --target /data/vms`

5. Start pool and Mark as autostart <br>
`virsh pool-start isos`<br>
`virsh pool-start vms`<br>
`virsh pool-autostart isos`<br>
`virsh pool-autostart vms`<br>

**SETUP TERRAFORM**
1. install yum-config-manager to manage the repository<br>
`yum install -y yum-utils git`
2. Use yum-config-manager to add official HashiCorp linux repository<br>
`yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo`
3. Install Terraform<br>
`yum -y install terraform`
4. Verify the installation<br>
`terraform -v`
5. Create directory for provider-libvirt binary<br>
`mkdir -p ~/.local/share/terraform/plugins/registry.terraform.io/dmacvicar/libvirt/0.6.2/linux_amd64`
6. Clone tfgen to pick provider file<br>
`git clone https://go.btech.id/arya/tfgen.git`
7. move provider-libvirt to directory<br>
`mv tfgen/provider/terraform-provider-libvirt ~/.local/share/terraform/plugins/registry.terraform.io/dmacvicar/libvirt/0.6.2/linux_amd64`