
1. Konfigurasi networking.
```linux
root# dnf update –y 
root# dnf install epel-release –y 
root# dnf makecache 
root# dnf install vim htop –y 
root# vim /etc/sysconfig/network-scripts/ifcfg-eno1 
... 
TYPE=Ethernet  
NAME=eno1  
DEVICE=eno1  
ONBOOT=yes  
IPADDR=10.2.2.1X  
PREFIX=24  
GATEWAY=10.2.2.1  
DNS1=8.8.8.8 
PROXY_METHOD=none  
BROWSER_ONLY=no  
BOOTPROTO=none  
DEFROUTE=yes  
IPV4_FAILURE_FATAL=no  
... 
Save and exit

root# systemctl restart NetworkManager 
root# ip addr show eno1  
```
> * eno1 adalah nama port ethernet 

2. Konfigurasi hostname.
```linux
root# hostnamectl set-hostname adwX 
root# vim /etc/hosts 
... 
202.65.116.10   adw1 
10.2.2.12 adw2 
10.2.2.13 adw3 
10.2.2.14 adw4 
10.2.2.15 adw5 
10.2.2.16 adw6 
10.2.2.17 adw7 
10.2.2.18 adw8 
... 
Save and exit 

root# ping adwX 
```
> * X adalah nomor, sesuaikan dengan hostname server 

3. Konfigurasi SSH.
```linux
root# ssh-keygen 
root# ssh-copy-id adwX 
root# ssh adwX hostname 
```