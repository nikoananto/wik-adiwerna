## Pra-instalasi 

1. Unduh Centos 8 Stream boot iso dari link [ini](https://www.centos.org/centos-stream/#download). 

2. Buat bootable USB drive dari iso tersebut. 

## instalasi 

1. Boot server dari USB drive, pilih Install CentOS Stream 8-Stream. <br/>
![centos-1](uploads/fd99f893dad89ce8265425bae8035d35/centos-1.png){width=75%} <br/>
 

2. Pilih English untuk pilihan bahasa, kemudian tekan Continue. <br/>
![centos-2](uploads/06d732340333c3ec0d010e55a4580ff8/centos-2.png){width=75%} <br/>


3. Di tampilan Installation Summary, ada beberapa konfigurasi yang harus di buat. <br/>
![centos-3](uploads/e9d8bdeee8e914ef4f9b73850787e699/centos-3.png){width=75%} <br/>
 

4. Pilih Network & Host Name. Hidupkan interface ethernet  yang terkoneksi dengan internet. Pilih Configure... untuk mengubah konfigurasi IP address ke static dan pastikan terkonfigurasi dengan agar terhubung dengan internet. <br/>
![centos-3-1](uploads/17f9968a2e04d3f5f59a3f5202b42a36/centos-3-1.png){width=75%} <br/>

 
5. Jika server sudah terhubung internet, menu Installation Source akan otomatis mengkonfigurasi dan memilih repositori centos untuk mengunduh paket yang diperlukan dalam proses instalasi. Sambil menunggu, pilih menu Installation Destination untuk mengkonfigurasi partisi disk yang diperlukan. Storage Configuration pilih Custom, kemudian pilih Done. <br/>
![Screenshot_20221223_215236](uploads/b2f9e6fb25729e78ae64af7bf27469cc/Screenshot_20221223_215236.png){width=75%} <br/>
 

6. Buat partisi yang dibutuhkan, 1 partisi untuk OS boot, 1 partisi root untuk data OS dan 1 partisi swap. Jika Server sudah support UEFI firmware, partisi OS boot di kaitkan di (/boot/efi), jika belum support kaitkan partisi di (/boot). <br/>
![Screenshot_20221223_232139](uploads/295a3515424086be881486a29c2482c4/Screenshot_20221223_232139.png){width=75%} <br/>


7. Di menu Software Selection, pilih Server. <br/>
![centos-3-4](uploads/8cf768b66938f953986de0fa5b6925e0/centos-3-4.png){width=75%} <br/>
 

8. Di menu Time & Date, klik peta lokasi Jakarta. <br/>
![centos-3-5](uploads/08e8831bca486e373ad5c2116a216c93/centos-3-5.png){width=75%} <br/>
 

9. Buat password untuk user root di menu Root Password. <br/>
![centos-3-6](uploads/2d781ebcd49590962e06b1b1093cda29/centos-3-6.png){width=75%} <br/>


10. Buat user reguler di menu Create User. <br/>
 ![centos-3-7](uploads/9f83c90f7458a632eb9e4966e48c2c7a/centos-3-7.png){width=75%} <br/>
 

11. Gambar di bawah ini tampilan jika semua menu sudah di konfigurasikan. <br/>
![centos-4](uploads/744cc1c761767ee1dbece2f508f33ef5/centos-4.png){width=75%} <br/>


12. Tekan Begin Installation untuk memulai proses installasi dan tunggu hingga selesai seperti di bawah ini. <br/> 
![centos-5](uploads/2fb038bc7464b5cccc59a3849985a548/centos-5.png){width=75%} <br/>
 

13. Tekan Reboot System. Tunggu server booting, jika muncul tampilan login seperti di bawah ini, Instalasi the Centos 8 Stream telah berhasil. <br/>
![centos-6](uploads/40e393b8ca41ad30f3b2751a593cb148/centos-6.png){width=75%}

 