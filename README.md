### Dokumentasi

1. [Instalasi OS](./Baremetal-OS-Installation.md)
2. [Basic Host Configuration](./Basic-Host-Configuration.md)
3. [Setup KVM dan Terraform](./Setup-KVM-dan-Terraform.md)
4. [Provisioning VM](./Provisioning-VM.md)
5. [Scoring System](https://gitlab.com/tjkt-adb/scoring-excel)
6. [Lab Script](./Lab-Script.md)
7. [Openshift CodeReady](./Setup-Openshift-CodeReady.md)
